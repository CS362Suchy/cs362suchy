#!/bin/bash

#Hopefully we will read some parameters for this script from the command line.

echo "To Use: ./ ArgumentsExample.sh Thing1 Thing2 Thing3 etc"

POSPAR1="$1"
POSPAR2="$2"
POSPAR3="$3"


echo "$1 is the first position parameter. \$1."

echo "$2 is the first position parameter. \$2."

echo "$3 is the first position parameter. \$3."

echo "The total number of parameters is given by $#"
