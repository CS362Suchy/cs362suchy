#include <stdio.h>
#include <stdlib.h>
/* 
 * This is a c program to create a red and blue plain pixel map
 * Usage:
 *
 * RedBluePPM OutFileName  numRows numCols
 *
 * Here our program takes and outfile name and  two arguments,
 *  the rows and columns for the size of the image 
*/
int main(int argc, char *argv[]){

    int numRows; 
    int numCols;

    printf("\n<$===$===$===$===$===$===$===$===$>\n");  // This is a single line comment
    printf("  I'm making a plain Pixel Map!\n");
    printf("<$===$===$===$===$===$===$===$===$>\n\n");

    if(argc!=4){
        printf("Usage: ./RedBluePPM OutFileName numRow numCols \n");
        exit(1);
    }
    if ((numRows =  atoi(argv[2])) <= 0){
        printf("Error: numRows needs to be positive");
    }

    return 0;
}
