#include <stdio.h>
#include <stdlib.h>
/* This is a C program that does something cool
 * Multi line comment
*/
int main(int argc, char *argv[]){

    int numRows;
    int numCols;
    int imageSize;
    int row, col;
    unsigned char *outimage;
    unsigned char *ptr;
    unsigned char *outoutFP;

    printf("\n<$===$===$===$===$>\n");  // This is a single line comment
    printf("  Hello World >:D\n");
    printf("<$===$===$===$===$>\n\n");
    
    if(argc!=4){
	printf("Usage: ./RedBluePPM OUTfileName numrow numcols \n");
	exit(1);
    }
    if ( (numRows = atoi(argv[2]) ) <= 0){
	printf("Error: numRows needs to be positive");
    }
    if ( (numCols = atoi(argv[3]) ) <= 0){
        printf("Error: numCols needs to be positive");
    }

    imageSize = numRows*numCols*3;
    outImage = (unsigned char *) malloc(imageSize);


    if (outputFP = fopen(argv[1], "w") == NULL){
	perror("output open error");
        printf("Error: can not open  output file\n");
        exit(1);
    }

    ptr = outImage;
    for(row = 0; row < numRows; row ++){
	for(col = 0; col < numCols; col++){
    	    if (col < numCols/2){
		*ptr = 255;
		*(ptr+1) = 0;
		*(ptr+2) = 0;
            }
            else {
		*ptr = 0;
		*(ptr+1) = 0;
		*(ptr+2) = 255;
            }
            ptr += 3;

        }
    }
   
    fprintf(outputFP, "P6 %d %d 225\n", numCols, numRows); 
    fwrite(outImage, 1, imageSize, outputFP);
    fclose(outputFP);
    return 0;
}
