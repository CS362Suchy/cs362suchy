import glob

import sys

"""This is a script for Assignment 5. It produces a time use report
   from a list of daily logs
"""

# Function that takes file as input  and sums the times within
def TimeCount(datafile):
    Data = []
    Hours = 0
    Minutes = 0

    for line in datafile.readlines():
        Data.append(line)

    Name = Data[0]
    Name = Name.strip() 
    Data = Data[1:]

    for line in Data:
        Day, Hour, Minute = line.split()	
        Hour = int(Hour[:1])
        Hours = Hours + Hour
	Minute = int(Minute[:2])
        Minutes = Minutes + Minute
 
    Hour, Minutes = divmod(Minutes, 60)
    Hours = Hours + Hour
    return Name, Hours, Minutes


file = open("Report","w")

# Main
for file2 in glob.glob("*.txt"):
    
    TimeData = open(file2,"r")

    Name, Hours, Minutes = TimeCount(TimeData)
    
    Final  = "Total time for " + Name + " : " + str(Hours) + " Hrs & " + str(Minutes) + " Mins\n\n"
    file = open("Report","a")
    file.write(Final)	
    
