#!/bin/bash
yourfilename='*.tex'
echo "start of log" > LatexCompileReport.txt

for entry  in $yourfilename
do
	pdflatex $entry
done

yourfile2='*.pdf'

for entry in $yourfile2
do
	wc -w  $entry >> LatexCompileReport.txt
done

rm *.log
rm *.aux

echo "Finished :)"
